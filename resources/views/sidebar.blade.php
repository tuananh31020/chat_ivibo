<!-- Sidebar Start -->
<aside class="sidebar">
    <!-- Tab Content Start -->
    <div class="tab-content">
        <!-- Chat Tab Content Start -->
        <div class="tab-pane active" id="chats-content">
            <div class="d-flex flex-column h-100">
                <div class="hide-scrollbar h-100" id="chatContactsList">

                    <!-- Chat Header Start -->
                    <div class="sidebar-header sticky-top p-2">

                        <div class="d-flex justify-content-between align-items-center">
                            <!-- Chat Tab Pane Title Start -->
                            <h5 class="font-weight-semibold mb-0">Chats</h5>
                            <!-- Chat Tab Pane Title End -->

                            <ul class="nav flex-nowrap">

                                <li class="nav-item list-inline-item d-block d-xl-none mr-1">
                                    <a class="nav-link text-muted px-1" href="#" title="Appbar" data-toggle-appbar="">
                                        <!-- Default :: Inline SVG -->
                                        <svg class="hw-20" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M4 6a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2H6a2 2 0 01-2-2V6zM14 6a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V6zM4 16a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2H6a2 2 0 01-2-2v-2zM14 16a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z"></path></svg>

                                        <!-- Alternate :: External File link -->
                                        <!-- <img class="hw-20" src="./../../assets/media/heroicons/outline/view-grid.svg" alt="" class="injectable hw-20"> -->
                                    </a>
                                </li>

                                <li class="nav-item list-inline-item mr-0">
                                    <div class="dropdown">
                                        <a class="nav-link text-muted px-1" href="#" role="button" title="Details" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <!-- Default :: Inline SVG -->
                                            <svg class="hw-20" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 5v.01M12 12v.01M12 19v.01M12 6a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2z"/>
                                            </svg>

                                            <!-- Alternate :: External File link -->
                                            <!-- <img src="./../../assets/media/heroicons/outline/dots-vertical.svg" alt="" class="injectable hw-20"> -->
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="#" role="button" data-toggle="modal" data-target="#startConversation">New Chat</a>
                                            <a class="dropdown-item" href="#" role="button" data-toggle="modal" data-target="#createGroup">Create Group</a>
                                            <a class="dropdown-item" href="#" role="button" data-toggle="modal" data-target="#inviteOthers">Invite Others</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>


                        <!-- Sidebar Header Start -->
                        <div class="sidebar-sub-header">
                            <!-- Sidebar Header Dropdown Start -->
                            <div class="dropdown mr-2">
                                <!-- Dropdown Button Start -->
                                <button class="btn btn-outline-default dropdown-toggle" type="button" data-chat-filter-list="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    All Chats
                                </button>
                                <!-- Dropdown Button End -->

                                <!-- Dropdown Menu Start -->
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" data-chat-filter="" data-select="all-chats" href="#">All Chats</a>
                                    <a class="dropdown-item" data-chat-filter="" data-select="friends" href="#">Friends</a>
                                    <a class="dropdown-item" data-chat-filter="" data-select="groups" href="#">Groups</a>
                                    <a class="dropdown-item" data-chat-filter="" data-select="unread" href="#">Unread</a>
                                    <a class="dropdown-item" data-chat-filter="" data-select="archived" href="#">Archived</a>
                                </div>
                                <!-- Dropdown Menu End -->
                            </div>
                            <!-- Sidebar Header Dropdown End -->

                            <!-- Sidebar Search Start -->
                            <form class="form-inline">
                                <div class="input-group">
                                    <input type="text" class="form-control search border-right-0 transparent-bg pr-0" placeholder="Search users">
                                    <div class="input-group-append">
                                        <div class="input-group-text transparent-bg border-left-0" role="button">
                                            <!-- Default :: Inline SVG -->
                                            <svg class="text-muted hw-20" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"/>
                                            </svg>

                                            <!-- Alternate :: External File link -->
                                            <!-- <img class="injectable hw-20" src="./../../assets/media/heroicons/outline/search.svg" alt=""> -->
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- Sidebar Search End -->
                        </div>
                        <!-- Sidebar Header End -->
                    </div>
                    <!-- Chat Header End -->

                    <!-- Chat Contact List Start -->
                    <ul class="contacts-list" id="chatContactTab" data-chat-list="">
                        <!-- Chat Item Start -->
                        <li class="contacts-item friends">
                            <a class="contacts-link" href="{{route('detail-1')}}">
                                <div class="avatar avatar-online">
                                    <img src="../../assets/media/avatar/2.png" alt="">
                                </div>
                                <div class="contacts-content">
                                    <div class="contacts-info">
                                        <h6 class="chat-name text-truncate">Catherine Richardson</h6>
                                        <div class="chat-time">Just now</div>
                                    </div>
                                    <div class="contacts-texts">
                                        <p class="text-truncate">I’m sorry, I didn’t catch that. Could you please repeat?</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- Chat Item End -->

                        <!-- Chat Item Start -->
                        <li class="contacts-item groups">
                            <a class="contacts-link"  href="{{route('detail-2')}}">
                                <div class="avatar bg-success text-light">
                                    <span>
                                        <!-- Default :: Inline SVG -->
                                        <svg class="hw-24" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"/>
                                        </svg>

                                        <!-- Alternate :: External File link -->
                                        <!-- <img class="injectable" src="./../../assets/media/heroicons/outline/user-group.svg" alt=""> -->
                                    </span>
                                </div>
                                <div class="contacts-content">
                                    <div class="contacts-info">
                                        <h6 class="chat-name">Themeforest Group</h6>
                                        <div class="chat-time"><span>10:20 pm</span></div>
                                    </div>
                                    <div class="contacts-texts">
                                        <p class="text-truncate"><span>Jeny: </span>That’s pretty common. I heard that a lot of people had the same experience.</p>
                                        <div class="d-inline-flex align-items-center ml-1">
                                            <!-- Default :: Inline SVG -->
                                            <svg class="hw-16 text-muted" viewBox="0 0 20 20" fill="currentColor">
                                                <path fill-rule="evenodd" d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z" clip-rule="evenodd"/>
                                            </svg>

                                            <!-- Alternate :: External File link -->
                                            <!-- <img class="injectable hw-16 text-muted" src="./../../assets/media/heroicons/solid/lock-closed.svg" alt=""> -->
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- Chat Item End -->

                    </ul>
                    <!-- Chat Contact List End -->
                </div>
            </div>
        </div>
        <!-- Chats Tab Content End -->
    </div>
    <!-- Tab Content End -->
</aside>
<!-- Sidebar End -->
