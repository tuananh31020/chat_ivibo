<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from prismthemes.com/quicky/ltr/light-skin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Jul 2021 01:59:29 GMT -->
<head>
    <meta charset="UTF-8">
    <!-- Viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
    <!-- SEO Meta Tags-->
    <meta name="keywords" content="quicky, chat, messenger, conversation, social, communication" />
    <meta name="description" content="Quicky is a Bootstrap based modern and fully responsive Messaging template to help build Messaging or Chat application fast and easy." />
    <meta name="subject" content="communication">
    <meta name="copyright" content="frontendmatters">
     <meta name="revised" content="Tuesday, November 10th, 2020, 08:00 am" />
    <title>Quicky - HTML Chat Template</title>
    <!-- Favicon and Touch Icons-->
    <link rel="apple-touch-icon" sizes="180x180" href="../../apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../favicon-16x16.png">
    <link rel="shortcut icon" href="http://prismthemes.com/quicky/favicon.ico" />
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="../../assets/webfonts/inter/inter.css">
    <link rel="stylesheet" href="../../assets/css/app.min.css">
</head>
